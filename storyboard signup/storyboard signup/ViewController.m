//
//  ViewController.m
//  storyboard signup
//
//  Created by Clicklabs 104 on 9/23/15.
//  Copyright (c) 2015 cl. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextField *appleid;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UIButton *signin;

@property (weak, nonatomic) IBOutlet UIButton *create;
@end

@implementation ViewController
@synthesize password;
@synthesize signin;
@synthesize appleid;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [password setSecureTextEntry: YES];
    [signin addTarget: self action : @selector(signin:) forControlEvents:(UIControlEventTouchUpInside)];
    // Do any additional setup after loading the view, typically from a nib.
}
-(void)signin:(UIButton *) sender{
    
    if ([appleid.text length] ==0)
    {
        UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"All fields are mandatory"  message:@"Enter your password"  preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }]];
        [self presentViewController:alertController animated:YES completion:nil];
    }
     else if ([password.text length] ==0)
    {
        UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"All fields are mandatory"  message:@"Enter your password"  preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }]];
        [self presentViewController:alertController animated:YES completion:nil];
    }
     else if ([self check: password.text ] == YES)
     {
         UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"wrong input password"  message:@"passsword should contain digits ,characters and special character(@,#,$,%,&) "  preferredStyle:UIAlertControllerStyleAlert];
         [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
             [self dismissViewControllerAnimated:YES completion:nil];
         }]];
         [self presentViewController:alertController animated:YES completion:nil];
         
     }
     else if ([password.text length] > 10)
     {
         UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"Password must be 8 characters long only "  message:nil preferredStyle:UIAlertControllerStyleAlert];
         [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
             [self dismissViewControllerAnimated:YES completion:nil];
         }]];
         [self presentViewController:alertController animated:YES completion:nil];
     }
    else{
        UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"signing in "  message:nil preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }]];
                [self presentViewController:alertController animated:YES completion:nil];
    }

}

-(BOOL) check: (NSString*) text{
    NSCharacterSet *set =[[NSCharacterSet characterSetWithCharactersInString:@"1234567890QWERTYUIOPLKJHGFDSAZXCVBNMqwertyuioplkjhgfdsazxcvbnm@#$%&"]invertedSet];
    if([text rangeOfCharacterFromSet:set].location== NSNotFound )
    {
        return NO;
    }
    else
    {
        return YES;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

//
//  AppDelegate.h
//  storyboard signup
//
//  Created by Clicklabs 104 on 9/23/15.
//  Copyright (c) 2015 cl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

